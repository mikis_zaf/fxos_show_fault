# -*- coding: utf-8 -*-
import re
"""
ver 1.11
Returns the output of 'show faults'.
"""

def faults_list(input_text):
    """a function that returns a list
    each list element is only the text between the 2 delimiters:
    delimiter 1: an line starting with the keyword 'Severity:'
    delimiter 2: a line containing the text 'Highest Severity:'
    """
    pattern = r"^Severity:.*?Highest Severity:.*?$"
    return re.findall(pattern, input_text, re.MULTILINE | re.DOTALL)


def my_dict(text_string):
    """a function that:
    1. gets as an input a text string
    2. parses all list elements looking for 2 regex patterns 
    The matches of each pattern create a list. At the end I have 2 lists
    
    The first regex matches any number of characters until a ':' is found
    The ':' is not matched, just it is used as a delimiter (possitive Lookahead)

    The second regex matches any character after the ':' until the end of each line
    3. returns a dictionary after combining the 2 lists
    """
    list_with_keys = re.findall(r'^.*?(?=:)', text_string, re.MULTILINE)
    list_with_values = re.findall(r'(?<=: ).*$', text_string, re.MULTILINE)
    return dict(zip(list_with_keys, list_with_values))


def show_fault_formatter(fault_list):
    """ 
    1. Gets as an input a list of dictionaries
    2. Walks through the list, gets specific keys from each list element and returns formatted list (each element has a specific width)
    """
    
    return_list = ["Severity  Code     Last Transition Time     ID       Description", "--------- -------- ------------------------ -------- -----------"]
    for i in fault_list:
        return_list.append('{:10}'.format(i['Severity'])+'{:9}'.format(i['Code'])+'{:25}'.format(i['Last Transition Time'])+'{:9}'.format(i['ID'])+'{:}'.format(i['Description']))
    return '\n'.join(return_list)


def severity(fault_list):
    """a function that:
    1. gets as an input a list of dictionaries
    2. parses specific key (Severity) creating a list
    3. creates a set from the list created in step 2
    4. if the set contains 'Critical' or 'Major' then it returns the string 'critical'
    In different case returns 'info'
    """
    
    list1 = []
    for i in fault_list:
        list1.append(i['Severity'])
    
    if ('Critical' in set(list1)) or ('Major' in set(list1)):
        sev = "critical"
    else:
        sev = "info"
    
    return sev



# the sam_techsupportinfo is a file with command outputs
with open('sam_techsupportinfo') as mikis_file:
    file_string = mikis_file.read()

# grab the commands
dict_keys = re.findall(r'(?<=`).*(?=`)', file_string, re.MULTILINE)

# grab the command outputs
dict_values = re.findall(r'(?<=`\n).*?(?=`)', file_string, re.MULTILINE | re.DOTALL)
for i in range(len(dict_values)):
    if i < len(dict_values):
        if "`s" in dict_values[i]:
            del dict_values[i]

# create a dictionary of command outputs
sam = dict(zip(dict_keys, dict_values))

try:
    # The sam[x].text.splitlines() returns a list where each line of the command output is a list element
    # first I create a list called 'show_fault_list' of all 'show fault severity detail' command outputs (list concatenation)
    list_of_commands = ["show fault severity critical detail", "show fault severity major detail", "show fault severity warning detail", "show fault severity minor detail", "show fault severity info detail", "show fault severity condition detail", "show fault severity cleared detail"]   
    show_fault_list = []
    for i in list_of_commands:
        show_fault_list += sam[i].splitlines()
    
    # Convert the list of faults to one string 'show_fault_string' and add an empty line after each list element
    show_fault_string = ""
    for j in show_fault_list:
        show_fault_string += j + '\n'
    
    # call the first function which takes the string of faults and creates a list of faults (each list element is one fault)
    list_of_faults = faults_list(show_fault_string)
    
    # call the second function and create a list of dictionaries. 
    # Each fault in the list is in a dictionary structure
    list_of_dictionaries=[]
    for i in list_of_faults:
        list_of_dictionaries.append(my_dict(i))
    
    # call the third function which takes the list of dictionaries and returns a formatted list
    res = show_fault_formatter(list_of_dictionaries)
    
    # call the forth function to adjust the signature severity ('CRITICAL' or 'INFO')
    severity_variable = severity(list_of_dictionaries)
    	
    # show the result
    print(res, "\n This is the severity: " + severity_variable)
except:
    print("Something is wrong with 'show fault severity * detail'")
 	

try:		   
    # try to read the sam_showtechsupport again, but this time look for 'show fault detail' (used by older FXOS releases)		   
    show_fault_detail = sam["show fault detail"].splitlines()
    
    # Convert the list of faults to one string 'show_fault_string' and add an empty line after each list element
    show_fault_string = ""
    for j in show_fault_detail:
        show_fault_string += j + '\n'
    	
    # call the first function which takes the string of faults and creates a list of faults (each list element is one fault)
    list_of_faults = faults_list(show_fault_string)
    
    # call the second function and create a list of dictionaries. 
    # Each fault in the list is in a dictionary structure
    list_of_dictionaries=[]
    for i in list_of_faults:
        list_of_dictionaries.append(my_dict(i))
    
    # call the third function which takes the list of dictionaries and returns a formatted list
    res = show_fault_formatter(list_of_dictionaries)
    
    # call the forth function to adjust the signature severity ('CRITICAL' or 'INFO')
    severity_variable = severity(list_of_dictionaries)
    	
    # show the result
    print(res, "\n This is the severity: " + severity_variable)
except:
    print("Something is wrong with 'show fault detail'")
  
    



